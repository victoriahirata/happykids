package br.com.happykids.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	public static Connection getConexao() throws SQLException, ClassNotFoundException {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://127.0.0.1:50536/browser/happykids";
		String user = "postgres";
		String password = "hirata";
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, user, password);
		return conn;
	}
}
