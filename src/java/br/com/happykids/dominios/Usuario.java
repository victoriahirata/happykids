package br.com.happykids.dominios;

public class Usuario extends EntidadeDominio {
	
	public String senha;
        public String email;

	public Usuario(String senha, String email) {
		super();
		this.senha = senha;
                this.email = email;
	}
	
	public Usuario() {

	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
        public String getEmail(){
            return email;
        }
        
        public void setEmail(String email){
            this.email = email;
        }
}
